#include <stdlib.h>

#include "cdynarr.h"

void *vector_init()
{
    vector_t *v = malloc(sizeof(void *) + 2 * sizeof(size_t));
    if (v)
    {
        v->data = NULL;
        v->count = 0;
        v->capacity = 0;
    }
    return v;
}

int vector_push_back(vector_t *v, void *data)
{
    if (v->capacity == v->count)
    {
        void *new = realloc(v->data, (v->count + 1) * sizeof(v->data[0]));
        if (new == NULL)
        {
            return 1;
        }
        v->data = new;
    }
    v->data[v->count] = data;
    ++v->count;
    ++v->capacity;
    return 0;
}

int vector_set(vector_t *v, unsigned int index, void *data)
{
    if (index >= v->count)
    {
        return 1;
    }
    v->data[index] = data;
    return 0;
}

int vector_delete(vector_t *v, unsigned int index)
{
    if (index >= v->count)
    {
        return 1;
    }
    v->data[index] = NULL;
    --v->count;
    vector_collapse(v);
    return 0;
}

void vector_collapse(vector_t *v)
{
    for (unsigned int i = 0; i < v->count;)
    {
        if (v->data[i] == NULL)
        {
            v->data[i] = v->data[i + 1];
            v->data[i + 1] = NULL;
        }
        else
        {
            ++i;
        }
    }
}

int vector_shrink(vector_t *v)
{
    vector_collapse(v);
    void *new = NULL;
    // Don't automatically free the vector's data pointer if the vector is empty
    if (v->count == 0)
    {
        new = realloc(v->data, sizeof(v->data[0]));
    }
    else
    {
        new = realloc(v->data, v->count * sizeof(v->data[0]));
    }
    if (new == NULL)
    {
        return 1;
    }
    v->data = new;
    v->capacity = v->count == 0 ? 1 : v->count;

    return 0;
}

void vector_teardown(vector_t *v) {
    free(v->data);
    free(v);
}
