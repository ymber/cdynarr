#ifndef CDYNARR_H
#define CDYNARR_H

typedef struct
{
    void **data;
    size_t count;
    size_t capacity;
} vector_t;

void *vector_init();
int vector_push_back(vector_t *v, void *data);
int vector_set(vector_t *v, unsigned int index, void *data);
int vector_delete(vector_t *v, unsigned int index);
void vector_collapse(vector_t *v);
int vector_shrink(vector_t *v);
void vector_teardown(vector_t *v);

#endif
