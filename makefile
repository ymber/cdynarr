VPATH=src

CC=clang
CFLAGS=-Wextra -Werror -fPIC -c
LDFLAGS=-shared

BUILD_DIR=$(abspath build)
SOURCES=$(notdir $(foreach dir,$(VPATH),$(wildcard $(dir)/*.c)))
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))
LIB=libcdynarr.a

DBG_BUILD_DIR=$(BUILD_DIR)/debug
DBG_OBJECTS=$(addprefix $(DBG_BUILD_DIR)/obj/, $(OBJECTS))
DBG_LIB=$(DBG_BUILD_DIR)/$(LIB)
DBG_CFLAGS=-g -O0

REL_BUILD_DIR=$(BUILD_DIR)/release
REL_OBJECTS=$(addprefix $(REL_BUILD_DIR)/obj/, $(OBJECTS))
REL_LIB=$(REL_BUILD_DIR)/$(LIB)
REL_CFLAGS=-O3

TEST_BUILD_DIR=$(BUILD_DIR)/test
TEST_SOURCES=$(wildcard $(abspath tests)/*.c)
TEST_LIBS=-lcmocka
TEST_EXECUTABLE=$(TEST_BUILD_DIR)/test_dynarr

debug: directories $(DBG_OBJECTS) $(DBG_LIB)
release: directories $(REL_OBJECTS) $(REL_LIB)
test: directories $(DBG_OBJECTS) $(TEST_EXECUTABLE)

directories:
	mkdir -p $(BUILD_DIR)
	mkdir -p $(DBG_BUILD_DIR)
	mkdir -p $(DBG_BUILD_DIR)/obj
	mkdir -p $(REL_BUILD_DIR)
	mkdir -p $(REL_BUILD_DIR)/obj
	mkdir -p $(TEST_BUILD_DIR)

$(DBG_OBJECTS): $(DBG_BUILD_DIR)/obj/%.o: %.c
	$(CC) $(CFLAGS) $(DBG_CFLAGS) $< -o $@

$(DBG_LIB): $(DBG_OBJECTS)
	ar rcs $(DBG_LIB) $(wildcard $(DBG_BUILD_DIR)/obj/*.o)

$(REL_OBJECTS): $(REL_BUILD_DIR)/obj/%.o: %.c
	$(CC) $(CFLAGS) $(REL_CFLAGS) $< -o $@

$(REL_LIB): $(REL_OBJECTS)
	ar rcs $(REL_LIB) $(wildcard $(REL_BUILD_DIR)/obj/*.o)

$(TEST_EXECUTABLE): $(DBG_OBJECTS)
	$(CC) $(TEST_SOURCES) $(TEST_LIBS) $(wildcard $(DBG_BUILD_DIR)/obj/*.o) -o $(TEST_EXECUTABLE)

clean:
	rm -rf $(BUILD_DIR)
