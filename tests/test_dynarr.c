#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>

#include <cmocka.h>

#include "../src/cdynarr.h"

void test_init(void **state)
{
    vector_t *v;
    v = vector_init();
    assert_ptr_equal(v->data, NULL);
    assert_int_equal(v->count, 0);
    assert_int_equal(v->capacity, 0);
    vector_teardown(v);
}

void test_push_back(void **state)
{
    vector_t *v;
    v = vector_init();
    int val = 42;
    assert_int_equal(vector_push_back(v, &val), 0);
    assert_int_equal(*(int *)v->data[0], val);
    assert_int_equal(v->count, 1);
    assert_int_equal(v->capacity, 1);
    vector_teardown(v);
}

void test_set(void **state)
{
    vector_t *v;
    v = vector_init();
    int val1 = 42;
    vector_push_back(v, &val1);
    int val2 = 314;
    vector_set(v, 0, &val2);
    assert_int_equal(*(int *)v->data[0], 314);
    vector_teardown(v);
}

void test_delete(void **state)
{
    vector_t *v;
    v = vector_init();
    int val1 = 42;
    vector_push_back(v, &val1);
    vector_delete(v, 0);
    assert_ptr_equal(v->data[0], NULL);
    assert_int_equal(v->count, 0);
    assert_int_equal(v->capacity, 1);
    vector_teardown(v);
}

void test_shrink(void **state)
{
    vector_t *v;
    v = vector_init();
    int val1 = 42;
    vector_push_back(v, &val1);
    int val2 = 314;
    vector_push_back(v, &val2);
    int val3 = 271;
    vector_push_back(v, &val3);
    vector_delete(v, 1);
    vector_shrink(v);
    assert_int_equal(v->count, 2);
    assert_int_equal(v->capacity, 2);
    assert_int_equal(*(int *)v->data[0], 42);
    assert_int_equal(*(int *)v->data[1], 271);
    vector_teardown(v);
}

int main()
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_init),
        cmocka_unit_test(test_push_back),
        cmocka_unit_test(test_set),
        cmocka_unit_test(test_delete),
        cmocka_unit_test(test_shrink)};

    return cmocka_run_group_tests(tests, NULL, NULL);
}
